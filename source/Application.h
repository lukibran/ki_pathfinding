#pragma once

#include "Header.h"
#include "Grid.h"
#include "Enemy.h"

class Application
{
public:
	Application(void);
	~Application(void);
	int run(const std::vector<CL_String> &args);
	

private:
	void esc(const CL_InputEvent &k, const CL_InputState &s);
	void exit();

	bool quit;
	float _lastUpdate;
	Grid m_grid;
	vector<Enemy*> enemies;

};
