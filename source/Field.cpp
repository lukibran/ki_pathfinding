#include "Field.h"
#include "Header.h"


CL_Font* Field::_font = 0;

Field::Field()
{
	_sprStart = NULL;
	_sprFinish = NULL;
}

Field::Field(std::map<int, CL_Sprite*>& states, Fieldtype type, int state, int x, int y) : _states(states), _type(type), _state(state), _x(x), _y(y), _g(0.0f), _h(0.0f)
{
	_yPos = y * FIELD_HEIGHT;
	_xPos = x * FIELD_WIDTH;
	_extraCost = 0;

	if(y % 2 != 0)
		_xPos += FIELD_WIDTH/2;

	_sprStart = NULL;
	_sprFinish = NULL;
}

Field::~Field()
{
}

void Field::Render(CL_GraphicContext &gc)
{
	_type.GetSprite()->draw(gc, _xPos, _yPos);

	if(_state > 0)
		_states[_state]->draw(gc, _xPos, _yPos);

	if(_sprStart != NULL)
		_sprStart->draw(gc, _xPos, _yPos);

	if(_sprFinish != NULL)
		_sprFinish->draw(gc, _xPos, _yPos);
}

void Field::RenderInfo(CL_GraphicContext &gc)
{
	if(Field::_font == 0)
	{
		Field::_font = new CL_Font(gc, "Arial", 14);
	}

	//DEBUG FONTS
	// Gesamkosten ausgeben 
	Field::_font->draw_text(gc, _xPos + 5, _yPos + FIELD_HEIGHT / 4 + (int)_font->get_font_metrics().get_height(), CL_StringHelp::float_to_local8(GetF(), 2), CL_Colorf::red);

	// Knoten Kosten ausgeben (rechts oben)
	Field::_font->draw_text(gc, _xPos + FIELD_WIDTH - (5 + _font->get_text_size(gc, CL_StringHelp::float_to_local8(_g, 2)).width), _yPos + FIELD_HEIGHT / 4 + (int)_font->get_font_metrics().get_height(),
		CL_StringHelp::float_to_local8(_g, 2), CL_Colorf::black);

	// Heuristik ausgeben (rechts unten)
	Field::_font->draw_text(gc, _xPos + FIELD_WIDTH - (5 + _font->get_text_size(gc, CL_StringHelp::float_to_local8(_h, 2)).width),
		_yPos + 3 * FIELD_HEIGHT / 4 + (int)_font->get_font_metrics().get_height() - 5, CL_StringHelp::float_to_local8(_h, 2), CL_Colorf::black);
}

void Field::AddNeighbor(Field* neighbor)
{
	_neighbors.push_back(neighbor);
}

std::vector<Field*> Field::GetNeighbors() const
{
	return _neighbors;
}

int Field::GetNeighborCount() const
{
	return _neighbors.size();
}

void Field::SetState(int state)
{
	_state = state;
}

int Field::GetState() const
{
	return _state;
}

int Field::GetScreenXPos(void)
{
	return _xPos;
}

int Field::GetScreenYPos(void)
{
	return _yPos;
}

int Field::GetXPos(void)
{
	return _x;
}

int Field::GetYPos(void)
{
	return _y;
}

Fieldtype& Field::GetType()
{
	return _type;
}

bool Field::operator<(const Field& rhs) const
{
	return rhs.GetF() < GetF();
}

void Field::SortNeighbors()
{
	std::sort(_neighbors.begin(), _neighbors.end());
}

void Field::SetG(float g)
{
	_g = g;
	//_f = _g + _h;
}

void Field::SetH(float h)
{
	_h = h;
	//_f = _g + _h;
}

float Field::GetG(void)
{
	return _g;
}

float Field::GetH(void)
{
	return _h;
}

float Field::GetF(void) const
{
	return _g + _h;
}

void Field::SetCameFrom(Field* field)
{
	_cameFrom = field;
}

Field* Field::getPrevious(void) const
{
	return _cameFrom;
}

int Field::GetCost(void) const
{
	return _type.GetWeight();
}

void Field::SetExtraCost(int cost)
{
	_extraCost = cost;
}

int Field::GetExtraCost(void) const
{
	return _extraCost;
}

void Field::SetStart(CL_Sprite* start)
{
	_sprStart = start;
}

void Field::SetFinish(CL_Sprite* finish)
{
	_sprFinish = finish;
}

int Field::GetFieldHeight()
{
	return FIELD_HEIGHT;
}

int Field::GetFieldWidth()
{
	return FIELD_WIDTH;
}