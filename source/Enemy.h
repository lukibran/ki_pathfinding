#pragma once

#include "Field.h"
#include "Grid.h"
#include "AStern.h"

#define ENEMY_DEBUG_STATE 0

class Enemy
{
public:

	Enemy(CL_GraphicContext &gc, Grid* grid, Field* start, int preis);
	~Enemy();

	void Render(CL_GraphicContext &gc);
	void Update();

	bool calcWay();

private:
	Grid* grid;
	Field* start;
	Field* pos;

	CL_Sprite sprite;	
	CL_Vec2<float> screenPos;
	int preis;
};

