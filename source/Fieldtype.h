#pragma once

#include "Header.h"

class Fieldtype
{
public:
	Fieldtype();
	Fieldtype(CL_Sprite* sprite, int weight);
	~Fieldtype();
	CL_Sprite* GetSprite();
	int GetWeight() const;

private:
	CL_Sprite* _sprite;
	int _weight;
};

