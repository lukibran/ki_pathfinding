#pragma once

#include "Fieldtype.h"


#define FIELD_WIDTH 64
#define FIELD_HEIGHT 48

class Field
{
public:

	Field();
	Field(std::map<int, CL_Sprite*>& states, Fieldtype type, int state, int x, int y);
	~Field();
	void Render(CL_GraphicContext &gc);
	void RenderInfo(CL_GraphicContext &gc);
	void AddNeighbor(Field* neighbor);
	int GetNeighborCount() const;
	std::vector<Field*> Field::GetNeighbors() const;
	void SetState(int state);
	int GetState() const;
	int GetScreenXPos(void);
	int GetScreenYPos(void);
	int GetXPos(void);
	int GetYPos(void);
	Fieldtype& GetType();
	bool operator<(const Field& rhs) const;
	void SortNeighbors();
	void SetG(float g);
	void SetH(float h);
	float GetG(void);
	float GetH(void);
	float GetF(void) const;
	int GetCost(void) const;
	int GetExtraCost(void) const;
	void SetExtraCost(int preis);
	void SetCameFrom(Field* field);
	Field* getPrevious(void) const;
	void SetStart(CL_Sprite* start);
	void SetFinish(CL_Sprite* finish);

	static int GetFieldHeight();
	static int GetFieldWidth();

private:
	std::map<int, CL_Sprite*> _states;
	Fieldtype _type;
	int _state;
	int _x;
	int _y;
	float _g;
	float _h;
	
	int _xPos;
	int _yPos;
	std::vector<Field*> _neighbors;
	static CL_Font* _font;

	Field* _cameFrom;
	int _extraCost;

	CL_Sprite* _sprStart;
	CL_Sprite* _sprFinish;
};


