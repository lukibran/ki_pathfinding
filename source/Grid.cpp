#include "Grid.h"
#include "Header.h"


Grid::Grid(void)
{
}

void Grid::Init(CL_GraphicContext &gc)
{
	_sprStart = CL_Sprite(gc, "Resources/start.png");
	_sprFinish = CL_Sprite(gc, "Resources/ziel.png");
}

Grid::~Grid()
{
}

void Grid::AddFieldType(string name, CL_Sprite* sprite, int weight)
{
	Fieldtype ft(sprite, weight);
	_fieldTypes[name] = ft;
}

Fieldtype Grid::GetFieldTypeByName(string name)
{
	return _fieldTypes[name];
}

void Grid::AddFieldState(int statenumber, CL_Sprite* sprite)
{
	_fieldStates[statenumber] = sprite;
}

CL_Sprite* Grid::GetFieldStateSpriteByNumber(int statenumber)
{
	return _fieldStates[statenumber];
}

void Grid::CreateGrid()
{
	Fieldtype ft;
	int length = 0;

	int sum[20][20] =  {{1,1,1,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,1,1},
						{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
						{1,2,2,2,1,1,1,2,1,2,1,2,1,2,1,2,1,2,1,1},
						{1,2,1,2,1,2,1,4,2,4,4,4,2,1,2,1,2,1,2,1},
						{2,1,2,1,2,1,2,4,4,4,4,4,1,1,1,1,1,1,1,1},
						{2,1,2,1,2,1,1,4,4,4,2,1,2,1,2,1,2,1,2,1},
						{1,1,1,1,2,2,2,2,2,2,4,4,4,1,2,2,1,1,1,1},
						{2,1,2,1,1,2,1,2,1,2,1,2,1,2,1,1,2,1,2,1},
						{2,1,2,1,2,1,2,1,2,1,2,1,2,2,2,1,1,1,1,1},
						{1,2,1,2,1,2,1,2,1,1,2,1,1,2,1,2,1,1,1,1},
						{1,2,1,2,4,4,4,4,4,4,4,1,1,1,1,2,1,2,1,1},
						{2,1,4,4,4,4,4,4,4,1,2,1,2,1,2,1,2,1,2,1},
						{4,2,4,2,4,4,4,4,2,1,2,1,2,1,2,1,2,1,2,1},
						{2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,4,4,4,4},
						{2,1,1,2,1,2,1,2,1,2,1,2,1,1,2,1,2,4,4,2},
						{2,1,1,1,1,1,1,2,1,2,1,2,1,1,2,1,1,2,1,1},
						{2,1,2,1,2,1,2,1,2,1,1,1,1,1,2,2,2,2,2,2},
						{1,2,1,2,1,4,4,4,4,4,4,4,4,2,1,2,1,1,1,1},
						{2,1,2,1,2,4,4,4,4,4,4,1,1,1,1,1,2,1,2,1},
						{2,1,2,1,1,2,1,4,4,4,4,1,2,2,1,2,1,2,1,1}};

	for(int y = 0; y < GRID_HEIGHT; ++y)
	{ 
		
		length = 0;

		if(y % 2 == 0)
		{
			length = GRID_WIDTH;
		}
		else
		{
			length = GRID_WIDTH - 1;
		}

		for(int x = 0; x < length; ++x)
		{
			if(sum[y][x] == 1)
				ft = _fieldTypes["Asphalt"];
			if(sum[y][x] == 2)
				ft = _fieldTypes["Steinig"];
			if(sum[y][x] == 4)
				ft = _fieldTypes["Impassable"];

			_fields[x][y] = Field(_fieldStates, ft, _defaultFieldState, x, y);
		}
	}
}

void Grid::GenerateNeighborList()
{
	for(int y = 0; y < GRID_HEIGHT; ++y)
	{
		int length = 0;
		bool isEven;

		if(y % 2 == 0)
		{
			length = GRID_WIDTH;
			isEven = true;
		}
		else
		{
			length = GRID_WIDTH - 1;
			isEven = false;
		}

		for(int x = 0; x < length; ++x)
		{
			//seitliche nachbarn
			if(x > 0)
				_fields[x][y].AddNeighbor(&_fields[x-1][y]);

			if(x < length - 1)
				_fields[x][y].AddNeighbor(&_fields[x+1][y]);

			//obere nachbarn
			if(y > 0)
			{
				 //rechts bei geradem, links bei ungeradem y-wert

				if(isEven) //gerade
				{
					if(x > 0) //links oben
					{
						_fields[x][y].AddNeighbor(&_fields[x-1][y-1]);
					}

					if(x < length - 1) //rechts oben
					{
						_fields[x][y].AddNeighbor(&_fields[x][y-1]);
					}
				}
				else
				{
					_fields[x][y].AddNeighbor(&_fields[x][y-1]); //links oben

					if(x < length) //rechts oben
					{
						_fields[x][y].AddNeighbor(&_fields[x+1][y-1]);
					}
				}
			}

			//untere nachbarn
			if(y < GRID_HEIGHT - 1)
			{
				if(isEven) //gerade
				{
					if(x > 0) //links unten
					{
						_fields[x][y].AddNeighbor(&_fields[x-1][y+1]);
					}

					if(x < length - 1) //rechts unten
					{
						_fields[x][y].AddNeighbor(&_fields[x][y+1]);
					}
				}
				else
				{
					_fields[x][y].AddNeighbor(&_fields[x][y+1]); // links unten

					if(x < length) //rechts unten
					{
						_fields[x][y].AddNeighbor(&_fields[x+1][y+1]);
					}
				}
			}
		}
	}
}

void Grid::Render(CL_GraphicContext &gc)
{
	for(int y = 0; y < GRID_HEIGHT; ++y)
	{
		int length = 0;

		if(y % 2 == 0)
		{
			length = GRID_WIDTH;
		}
		else
		{
			length = GRID_WIDTH - 1;
		}

		for(int x = 0; x < length; ++x)
		{
			_fields[x][y].Render(gc);
		}
	}
}

Field& Grid::GetField(int x, int y)
{
	return _fields[x][y];
}

void Grid::SetDefaultFieldState(int state)
{
	_defaultFieldState = state;
}

void Grid::SetStart(int x, int y, bool isStart)
{
	if(isStart)
	{
		_fields[x][y].SetStart(&_sprStart);
	}
	else
	{
		_fields[x][y].SetStart(NULL);
	}
}

void Grid::SetFinish(int x, int y, bool isFinish)
{
	if(isFinish)
	{
		_fields[x][y].SetFinish(&_sprFinish);
	}
	else
	{
		_fields[x][y].SetFinish(NULL);
	}
}