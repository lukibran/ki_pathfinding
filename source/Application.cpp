#include "Application.h"
#include "Grid.h"
#include "Enemy.h"
#include "Player.h"

Application::Application(void)
{
}


Application::~Application(void)
{
}

int Application::run(const std::vector<CL_String> &args)
{
	quit = false;
	bool start = false;

	CL_DisplayWindowDescription desc;
	desc.set_title("A* Pathfinding gs13m011");
	desc.set_size(CL_Size(1280, 975), true);
	desc.set_allow_resize(true);

	CL_DisplayWindow window(desc);
	CL_InputContext ic = window.get_ic();
	CL_InputDevice keygrid = ic.get_keyboard();

	CL_Slot slot_quit = window.sig_window_close().connect(this, &Application::exit);
	CL_Slot slot_input_up = (window.get_ic().get_keyboard()).sig_key_up().connect(this, &Application::esc);
	CL_GraphicContext gc = window.get_gc();

	

	CL_Sprite asphalt(gc, "Resources/asphalt.png");
	CL_Sprite impassable(gc, "Resources/impassable.png");
	CL_Sprite bumpy(gc, "Resources/bumpy.png");
	CL_Sprite rahmen(gc, "Resources/rahmen.png");
	CL_Sprite pfad(gc, "Resources/pfad.png");

	m_grid.Init(gc);

	m_grid.AddFieldType("Asphalt", &asphalt, 1);
	m_grid.AddFieldType("Steinig", &bumpy, 5);
	m_grid.AddFieldType("Impassable", &impassable, -1);
	m_grid.AddFieldState(3, &rahmen);
	m_grid.AddFieldState(4, &pfad);

	m_grid.SetDefaultFieldState(0);

	m_grid.CreateGrid();
	m_grid.GenerateNeighborList();

	Player player = Player(gc, &m_grid, &m_grid.GetField(1,1), &m_grid.GetField(18,19));

	//F�r Umposierung der Gegener auskommentieren und untenstehende Kommentieren
#if 0
	Enemy opp1 = Enemy(gc, &m_grid, &m_grid.GetField(6,6), 100);
	Enemy opp2 = Enemy(gc, &m_grid, &m_grid.GetField(8,8), 100);
	Enemy opp3 = Enemy(gc, &m_grid, &m_grid.GetField(11,15), 100);
#endif

#if 0
	Enemy opp1 = Enemy(gc, &m_grid, &m_grid.GetField(6,6), 100);
	Enemy opp2 = Enemy(gc, &m_grid, &m_grid.GetField(8,8), 100);
	Enemy opp3 = Enemy(gc, &m_grid, &m_grid.GetField(16,14), 100);
#endif

#if 1
	Enemy e1 = Enemy(gc, &m_grid, &m_grid.GetField(14,14), 100);
	Enemy e2 = Enemy(gc, &m_grid, &m_grid.GetField(1,8), 100);
	Enemy e3 = Enemy(gc, &m_grid, &m_grid.GetField(8,2), 100);
#endif

	enemies.push_back(&e1);
	enemies.push_back(&e2);
	enemies.push_back(&e3);

	

	unsigned int last_time = CL_System::get_time();
	_lastUpdate = 0.0f;

	while (!quit)
	{
		unsigned int current_time = CL_System::get_time();
		float time_delta_ms = static_cast<float> (current_time - last_time);
		_lastUpdate += static_cast<int>(time_delta_ms);
		gc.clear(CL_Colorf(1.0f,0.0f,0.0f));

		if(_lastUpdate > 25.0f)
		{
			_lastUpdate = 0.0f;

			for (vector<Enemy*>::iterator it = enemies.begin(); it!=enemies.end(); ++it)
			{
				(*it)->Update();
			}
		}

			
		if(start == true || keygrid.get_keycode(CL_KEY_ENTER))
		{	
			player.CalculateWay();
			start = true;
		}
		

		m_grid.Render(gc);
		player.Render(gc);
		
		for (vector<Enemy*>::iterator it = enemies.begin(); it!=enemies.end(); ++it)
		{
			(*it)->Render(gc);
		}

		window.flip(1);

		CL_KeepAlive::process(0);

		last_time = current_time;
	}
	
	return 0;
}

void Application::esc(const CL_InputEvent &k, const CL_InputState &s)
{
	if(k.id == CL_KEY_ESCAPE)
	{
		quit = true;
	}
}

	
void Application::exit()
{
	quit = true;
}