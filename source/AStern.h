#pragma once

#include <vector>
#include <algorithm>
#include <assert.h>

#include "Field.h"
#include "Grid.h"

class AStern
{
public:
	AStern(void);
	~AStern(void);
	static bool calcWay(Field* start, Field* ziel, vector<Field*>* todoList, vector<Field*>* finalList, vector<Field*>* calculatedWay, bool setState, bool ignoreExtraCost);

private:
	static void checkPoint(Field* field, Field* ziel, vector<Field*>* todoList, vector<Field*>* finalList, bool setState, bool ignoreExtraCost);
};

