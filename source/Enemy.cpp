#include "Enemy.h"

Enemy::Enemy(CL_GraphicContext &gc, Grid* grid, Field* start, int preis) : grid(grid)
{
	sprite = CL_Sprite(gc, "Resources/enemy.png");

	
	start = start;
	pos = start;

	preis = preis;

	screenPos.x = static_cast<float>(pos->GetScreenXPos());
	screenPos.y = static_cast<float>(pos->GetScreenYPos());

	// Preis auf Position erh�hen
	pos->SetExtraCost(pos->GetExtraCost() + preis);

	// Preis der Nachbaren erh�hen 
	vector<Field*> neighbors = pos->GetNeighbors();
	for (vector<Field*>::iterator it = neighbors.begin(); it!=neighbors.end(); ++it)
	{
		(*it)->SetExtraCost((*it)->GetExtraCost() + preis);
	}
}

Enemy::~Enemy()
{
}

void Enemy::Render(CL_GraphicContext &gc)
{
	sprite.draw(gc, screenPos.x, screenPos.y);
}

void Enemy::Update()
{

}



