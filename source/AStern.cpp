#include "AStern.h"
#include "Header.h"
#include <algorithm>

AStern::AStern(void)
{
}

AStern::~AStern(void)
{
}

bool compare(Field* f1, Field* f2)
{
	return *f1 < *f2;
}

bool AStern::calcWay(Field* start, Field* ziel, vector<Field*>* todoList, vector<Field*>* finalList, vector<Field*>* calculatedWay, bool setState, bool ignoreExtraCost)
{
	Field* m_field;

	// add the first node to the opn list
	start->SetG(0);
	todoList->push_back(start);

	if(setState)
	{
		start->SetState(3); //known
	}

	// Solange die TodoListe nicht Leer ist wird diese while Schleife ausgef�hrt
	while(todoList->size() > 0)
	{
		
		m_field = todoList->back();
		todoList->pop_back();

		// Hier wird nach dem Ziel gesucht 
		if(m_field == ziel)
		{
			//Durch R�ckverfolgung wird der Pfad gesetzt
			//SetState 4 ist der Gelbe Pfad
			calculatedWay->clear();

			while(m_field != start)
			{
				calculatedWay->push_back(m_field);

				if(setState)
				{
					m_field->SetState(4); 
				}

				m_field = m_field->getPrevious();
			}

			return true;
		}

		//Aufrufen der Funktion CheckPoint in der die Gesamtkosten berechnent werden
		checkPoint(m_field, ziel, todoList, finalList, setState, ignoreExtraCost);

		//wurde ein Knoten gefunden ab damit in die finalList
		finalList->push_back(m_field);

		if(setState)
		{
			m_field->SetState(3);
		}

		// TodoList nach Gesamtkosten Sortiert
		sort(todoList->begin(), todoList->end(), compare);
	}

	return false;
}

void AStern::checkPoint(Field* field, Field* ziel, vector<Field*>* todoList, vector<Field*>* finalList, bool setState, bool ignoreExtraCost)
{
	vector<Field*> neighbors = field->GetNeighbors();
	float m_fieldCost = 0.0f;

	//Kosten des Derzeitigen Feldes in m_fieldCost gespeichert
	if(ignoreExtraCost)
	{
		m_fieldCost = field->GetType().GetWeight();
	}
	else
	{
		m_fieldCost = (field->GetType().GetWeight() + field->GetExtraCost());
	}

	for (vector<Field*>::iterator it = neighbors.begin(); it!=neighbors.end(); ++it)
	{
		// Es werden alle Nachbaren Durchgegangen und geschaut ob sie in der 
		// Finalized Liste sind
		if(std::find(finalList->begin(), finalList->end(), (*it)) == finalList->end())
		{
			// Auf Hindernis (-1) �berpr�fen
			if((*it)->GetType().GetWeight() > 0)
			{
				//Berechnen von Delta X und DeltaY um die Heuristik
				//des Knotens bestimmen zu k�nnen
				int dx = abs((*it)->GetXPos() - ziel->GetXPos());
				int dy = abs((*it)->GetYPos() - ziel->GetYPos());

				if((*it)->GetYPos() != ziel->GetYPos())
				{
					if((*it)->GetYPos() % 2 == ziel->GetYPos() % 2)
					{
						--dy;
					}
				}

				// Zum berechnen der heuristik wird die Manhattan Methode verwendet
				float heu = static_cast<float>(dx + dy);

				// Als Feldkosten wird nur das derzeitige Feld genommen 
				float finCost = m_fieldCost + heu;

				vector<Field*>::iterator openMatch;
				openMatch = std::find(todoList->begin(), todoList->end(), (*it));

				// Knoten wieder in die todoliste Pushen
				if(openMatch == todoList->end())
				{
					todoList->push_back(*it);
					(*it)->SetG(m_fieldCost + field->GetG());
					(*it)->SetH(heu);
					(*it)->SetCameFrom(field);
				}
				else
				{
					// Abfrage ob anderer Knoten weniger Kosten als 
					// jetziger hat
					if(finCost < (*it)->GetF())
					{
						(*it)->SetG(m_fieldCost + field->GetG());
						(*it)->SetH(heu);
						(*it)->SetCameFrom(field);
					}
				}
			}
		}
	}
}