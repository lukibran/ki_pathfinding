#include "Header.h"
#include "Application.h"

int main(const std::vector<CL_String> &args)
{
	try
	{
		// Initialisieren der Clanlib
		CL_SetupCore setup_core;

		//Initialisieren der Clanlib Display Komponenten
		CL_SetupDisplay setup_display;

		CL_SetupSWRender setup_swrender;

		Application app;
		int ret=app.run(args);
		return ret;

		
	}
	catch(CL_Exception &exception)
	{
		// Create a console window for text-output if not available
		CL_ConsoleWindow console("Console", 80, 160);
		CL_Console::write_line("Exception caught: " + exception.get_message_and_stack_trace());
		console.display_close_message();

		return -1;
	}
}

CL_ClanApplication app(main);