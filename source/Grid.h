#pragma once

#include "FieldType.h"
#include "Field.h"

#define GRID_WIDTH 20 
#define GRID_HEIGHT 20	

using namespace std;

typedef map<string, Fieldtype> FieldTypes;
typedef map<int, CL_Sprite*> FieldStates;

class Grid
{
public:
	Grid(void);
	~Grid(void);

	void Init(CL_GraphicContext &gc);
	void AddFieldType(string name, CL_Sprite* sprite, int weight);
	Fieldtype GetFieldTypeByName(string name);
	void AddFieldState(int statenumber, CL_Sprite* sprite);
	CL_Sprite* GetFieldStateSpriteByNumber(int statenumber);
	void CreateGrid();
	void GenerateNeighborList();
	void Render(CL_GraphicContext &gc);
	Field& GetField(int x, int y);
	void SetDefaultFieldState(int state);
	void SetStart(int x, int y, bool isStart);
	void SetFinish(int x, int y, bool isFinish);

private:
	Field _fields[GRID_WIDTH][GRID_HEIGHT];

	FieldTypes _fieldTypes;
	FieldStates _fieldStates;
	int _defaultFieldState;
	vector<char *> m_Grid;

	CL_Sprite _sprStart;
	CL_Sprite _sprFinish;
};

