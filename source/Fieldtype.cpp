#include "Fieldtype.h"


Fieldtype::Fieldtype()
{
}

Fieldtype::Fieldtype(CL_Sprite* sprite, int weight): _sprite(sprite), _weight(weight)
{
}


Fieldtype::~Fieldtype()
{
}

CL_Sprite* Fieldtype::GetSprite()
{
	return _sprite;
}

int Fieldtype::GetWeight() const
{
	return _weight;
}