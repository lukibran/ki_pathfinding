#include "Player.h"
#include "AStern.h"


Player::Player(CL_GraphicContext &gc, Grid* board, Field* start, Field* ziel)
{

	_board = board;

	
	_start = start;
	_ziel = ziel;

	_board->SetStart(_start->GetXPos(), _start->GetYPos(), true);
	_board->SetFinish(_ziel->GetXPos(), _ziel->GetYPos(), true);

	_position = _start;
	_position->SetState(3);

	_finalList.push_back(_position);
}

Player::~Player()
{
}

void Player::Render(CL_GraphicContext &gc)
{
	
	// render closed info
	for (vector<Field*>::iterator it = _finalList.begin(); it!=_finalList.end(); ++it)
	{
		if((*it) != _position && (*it) != _ziel)
		{
			(*it)->RenderInfo(gc);
		}
	}

	// redner way
	for (vector<Field*>::iterator it = _calculatedWay.begin(); it!=_calculatedWay.end(); ++it)
	{
		if((*it) != _position && (*it) != _ziel)
		{
			(*it)->RenderInfo(gc);
		}
	}
}

void Player::Update()
{
	CalculateWay();
}

bool Player::CalculateWay()
{
	return AStern::calcWay(_position, _ziel, &_todoList, &_finalList, &_calculatedWay, ROUTE_DEBUG_STATE, false);
}
