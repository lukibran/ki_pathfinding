#pragma once

#include "Grid.h"
#include "Field.h"
#include "AStern.h"

#define ROUTE_DEBUG_STATE 1

class Player
{
public:
	Player(CL_GraphicContext &gc, Grid* board, Field* start, Field* ziel);
	~Player();

	void Render(CL_GraphicContext &gc);
	void Update();
	bool CalculateWay();

private:
	Grid* _board;
	Field* _start;
	Field* _ziel;
	Field* _position;

	vector<Field*> _todoList;
	vector<Field*> _finalList;
	vector<Field*> _calculatedWay;
};

